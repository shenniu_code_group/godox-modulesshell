﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using ShenNius.Repository;
using ShenNius.Sys.API.Domain.Entity;
using ShenNius.Sys.API.Domain.Repository;
using ShenNius.Sys.API.Domain.ValueObjects;
using ShenNius.Sys.API.Domain.ValueObjects.Enum;
using ShenNius.Sys.API.Dtos.Common;
using ShenNius.Sys.API.Dtos.Input;
using ShenNius.Sys.API.Infrastructure.Attributes;
using ShenNius.Sys.API.Infrastructure.Configs;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ShenNius.Sys.API.Controllers
{
    public class ConfigController : ApiControllerBase
    {
        private readonly IBaseRepository<Config> _configRepository;
        private readonly IMapper _mapper;
        private readonly IDistributedCache _cacheHelper;
        private readonly IRecycleRepository _recycleRepository;

        public ConfigController(IBaseRepository<Config> configRepository, IMapper mapper, IDistributedCache cacheHelper, IRecycleRepository recycleRepository)
        {
            _configRepository = configRepository;
            _mapper = mapper;
            _cacheHelper = cacheHelper;
            _recycleRepository = recycleRepository;
        }
        [HttpDelete, Authority]
        public async Task<ApiResult> Deletes([FromBody] DeletesInput commonDeleteInput)
        {
            return new ApiResult(await _configRepository.DeleteAsync(commonDeleteInput.Ids));
        }
        [HttpDelete, Authority(Action = nameof(BtnEnum.Recycle))]
        public virtual Task<ApiResult> SoftDelete([FromBody] DeletesInput deleteInput)
        {
            return _recycleRepository.SoftDeleteAsync(deleteInput, _configRepository);
        }
        [HttpGet, Authority]
        public async Task<ApiResult> GetListPages(int page, string key = null)
        {
            Expression<Func<Config, bool>> whereExpression = d => d.IsDeleted == false;
            if (!string.IsNullOrEmpty(key))
            {
                whereExpression = d => d.Value.Contains(key) && d.IsDeleted == false;
            }
            var res = await _configRepository.GetPagesAsync(page, 15, whereExpression, d => d.Id, false);
            return new ApiResult(data: new { count = res.TotalItems, items = res.Items });
        }
        [HttpGet, Authority]
        public async Task<ApiResult> Detail(int id)
        {
            var res = await _configRepository.GetModelAsync(d => d.Id == id && !d.IsDeleted);
            return new ApiResult(data: res);
        }

        [HttpPost, Authority]
        public async Task<ApiResult> Add([FromBody] ConfigInput input)
        {
            var model = await _configRepository.GetModelAsync(d => d.EnName.Equals(input.EnName) && !d.IsDeleted);
            if (model.Id > 0)
            {
                throw new ArgumentNullException("英文名称已存在");
            }
            var modelInput = _mapper.Map<Config>(input);
            var res = await _configRepository.AddAsync(modelInput);
            return new ApiResult(data: res);
        }
        [HttpPut, Authority]
        public async Task<ApiResult> Modify([FromBody] ConfigModifyInput input)
        {
            var model = await _configRepository.GetModelAsync(d => d.EnName.Equals(input.EnName) && d.Id != input.Id && !d.IsDeleted);
            if (model.Id > 0)
            {
                throw new ArgumentNullException("英文名称已存在");
            }
            model.Modify(input.Name, input.EnName, input.Type, input.Summary, input.Id);
            var res = await _configRepository.UpdateAsync(model);
            return new ApiResult(data: res);
        }
        [HttpPost, Authority]
        public ApiResult SettingValue([FromBody] SysSetting input)
        {
            SysSetting.WriteAsync(input);
            return new ApiResult();
        }
    }
}
