﻿using Microsoft.AspNetCore.Mvc;

namespace ShenNius.Sys.API.Controllers
{
    [Route("api/sys/[controller]/[action]")]
    [ApiController]
    public abstract class ApiControllerBase : ControllerBase
    {

    }
}
