﻿using AutoMapper;
using ShenNius.Sys.API.Domain.Entity;
using ShenNius.Sys.API.Dtos.Input;
using ShenNius.Sys.API.Dtos.Output;

namespace ShenNius.Sys.API
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {

            //sys
            CreateMap<User, LoginOutput>().ForMember(d => d.LoginName, s => s.MapFrom(i => i.Name));
            CreateMap<User, UserOutput>();
            CreateMap<UserRegisterInput, User>();

            CreateMap<RoleInput, Role>();
           
            CreateMap<RoleModifyInput, Role>();

            CreateMap<MenuModifyInput, Menu>();
            CreateMap<MenuInput, Menu>();
            //ParentMenuOutput
            CreateMap<Menu, ParentMenuOutput>();
            CreateMap<Menu, MenuAuthOutput>();

            CreateMap<ConfigInput, Config>().ForMember(d=>d.Value,s=>s.MapFrom(i=>i.Name));
            CreateMap<TenantInput, Tenant>();
            CreateMap<TenantModifyInput, Tenant>();
        }
    }
}
