﻿namespace ShenNius.Sys.API.Dtos.Input
{
    public class RoleModifyInput
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
