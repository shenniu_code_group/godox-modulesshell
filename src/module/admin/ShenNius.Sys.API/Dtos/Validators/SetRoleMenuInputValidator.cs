﻿using FluentValidation;
using ShenNius.Sys.API.Dtos.Input;

namespace ShenNius.Sys.API.Dtos.Validators
{
    public class SetRoleMenuInputValidator : AbstractValidator<SetRoleMenuInput>
    {
        public SetRoleMenuInputValidator()
        {
            RuleFor(x => x.RoleId).NotEmpty().WithMessage("用户id必须填写");
            RuleFor(x => x.MenuIds).NotEmpty().WithMessage("角色id至少填写一个");

        }
    }
}
