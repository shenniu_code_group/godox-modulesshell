﻿using FluentValidation;
using ShenNius.Sys.API.Dtos.Input;

namespace ShenNius.Sys.API.Dtos.Validators
{
    public class RoleInputValidator : AbstractValidator<RoleInput>
    {
        public RoleInputValidator()
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage("请填写角色名称");
            RuleFor(x => x.Description).NotEmpty().WithMessage("请填写角色备注");
        }
    }
}
