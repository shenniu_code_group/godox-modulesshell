﻿using FluentValidation;
using ShenNius.Sys.API.Dtos.Input;

namespace ShenNius.Sys.API.Dtos.Validators
{
    public class ModifyPwdInputValidator : AbstractValidator<ModifyPwdInput>
    {
        public ModifyPwdInputValidator()
        {
            // CascadeMode = CascadeMode.Stop;
            RuleFor(x => x.Id).NotEmpty().WithMessage("请填写用户Id");
            RuleFor(x => x.OldPassword).NotEmpty().WithMessage("请填写用户旧密码");
            RuleFor(x => x.NewPassword).NotEmpty().WithMessage("请填写用户新密码");
            RuleFor(x => x.ConfirmPassword).NotEmpty().WithMessage("确认密码必须填写");

        }
    }
}
