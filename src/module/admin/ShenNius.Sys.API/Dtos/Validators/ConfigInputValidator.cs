﻿using FluentValidation;
using ShenNius.Sys.API.Dtos.Input;

namespace ShenNius.Sys.API.Dtos.Validators
{
    public class ConfigInputValidator : AbstractValidator<ConfigInput>
    {
        public ConfigInputValidator()
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage("名称必须填写");
            RuleFor(x => x.Type).NotEmpty().WithMessage("类型必须填写");
        }
    }
}
