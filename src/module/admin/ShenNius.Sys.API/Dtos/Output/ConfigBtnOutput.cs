﻿namespace ShenNius.Sys.API.Dtos.Output
{
    public class ConfigBtnOutput
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Status { get; set; }
    }
}
