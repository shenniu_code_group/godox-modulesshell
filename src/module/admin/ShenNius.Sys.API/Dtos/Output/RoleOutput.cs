﻿namespace ShenNius.Sys.API.Dtos.Output
{
    public class RoleOutput
    {
        public int Id { get; set; }
        public string Name { get;  set; }
        public string Description { get;  set; }
        /// <summary>
        /// 是否已经授权
        /// </summary>
        public bool Status { get; set; }
    }
}
