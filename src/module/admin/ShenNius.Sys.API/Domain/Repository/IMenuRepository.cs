﻿using ShenNius.Repository;
using ShenNius.Sys.API.Domain.Entity;
using ShenNius.Sys.API.Dtos.Input;
using ShenNius.Sys.API.Dtos.Output;
using ShenNius.Sys.API.Infrastructure.Configs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ShenNius.Sys.API.Domain.Repository
{
    public interface IMenuRepository : IBaseRepository<Menu>
    {
        Task<ApiResult> BtnCodeByMenuIdAsync(int menuId, int roleId);
        Task<ApiResult> TreeRoleIdAsync(int roleId);

        Task<ApiResult> AddToUpdateAsync(MenuInput menuInput);
        Task<ApiResult> GetListPagesAsync(int page, string key = null);
        Task<ApiResult> ModifyAsync(MenuModifyInput menuModifyInput);

        Task<ApiResult> LoadLeftMenuTreesAsync(int userId);
        /// <summary>
        /// 根据当前用户Id获取所有角色关联的菜单id和按钮id
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<List<R_Role_Menu>> GetCurrentMenuByUser(int userId);
        /// <summary>
        /// 当前用户所有的权限集合
        /// </summary>
        /// <returns></returns>
        Task<List<MenuAuthOutput>> GetCurrentAuthMenus(int userId);
        Task<ApiResult> GetAllParentMenuAsync();
    }
}
