﻿using ShenNius.Sys.API.Domain.Entity.Common;
using SqlSugar;

/*************************************
* 类名：Tenant
* 作者：realyrare
* 邮箱：mahonggang8888@126.com
* 时间：2021/3/11 17:10:38
*┌───────────────────────────────────┐　    
*│　      版权所有：神牛软件　　　　	 │
*└───────────────────────────────────┘
**************************************/

namespace ShenNius.Sys.API.Domain.Entity
{
    [SugarTable("Sys_Tenant")]
    public class Tenant : BaseEntity, IAggregateRoot
    {

        /// <summary>
        /// Desc:系统用户ID
        /// Default:0
        /// Nullable:False
        /// </summary>
        public int UserId { get; private set; }

        /// <summary>
        /// Desc:网站名称
        /// Default:-
        /// Nullable:False
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Desc:网站域名
        /// Default:-
        /// Nullable:False
        /// </summary>
        public string Url { get; private set; }

        /// <summary>
        /// Desc:网站Logo
        /// Default:-
        /// Nullable:False
        /// </summary>
        public string Logo { get; private set; }

        /// <summary>
        /// Desc:网站描述
        /// Default:-
        /// Nullable:True
        /// </summary>
        public string Summary { get; private set; }

        /// <summary>
        /// Desc:公司电话
        /// Default:-
        /// Nullable:True
        /// </summary>
        public string Tel { get; private set; }

        /// <summary>
        /// Desc:公司传真
        /// Default:-
        /// Nullable:True
        /// </summary>
        public string Fax { get; private set; }

        /// <summary>
        /// Desc:公司人事邮箱
        /// Default:-
        /// Nullable:True
        /// </summary>
        public string Email { get; private set; }

        /// <summary>
        /// Desc:公司客服QQ
        /// Default:-
        /// Nullable:True
        /// </summary>
        public string QQ { get; private set; }

        /// <summary>
        /// Desc:微信公众号图片
        /// Default:-
        /// Nullable:True
        /// </summary>
        public string WeiXin { get; private set; }

        /// <summary>
        /// Desc:微博链接地址或者二维码
        /// Default:-
        /// Nullable:True
        /// </summary>
        public string WeiBo { get; private set; }

        /// <summary>
        /// Desc:公司地址
        /// Default:-
        /// Nullable:True
        /// </summary>
        public string Address { get; private set; }

        /// <summary>
        /// Desc:网站备案号其它等信息
        /// Default:-
        /// Nullable:True
        /// </summary>
        public string Code { get; private set; }

        /// <summary>
        /// Desc:网站SEO标题
        /// Default:-
        /// Nullable:False
        /// </summary>
        public string Title { get; private set; }

        /// <summary>
        /// Desc:网站SEO关键字
        /// Default:-
        /// Nullable:False
        /// </summary>
        public string Keyword { get; private set; }

        /// <summary>
        /// Desc:网站SEO描述
        /// Default:-
        /// Nullable:False
        /// </summary>
        public string Description { get; private set; }

        /// <summary>
        /// Desc:网站版权等信息
        /// Default:-
        /// Nullable:False
        /// </summary>
        public string Copyright { get; private set; }

        /// <summary>
        /// Desc:如果状态关闭，请输入关闭网站原因
        /// Default:-
        /// Nullable:True
        /// </summary>
        public string CloseInfo { get; private set; }

        /// <summary>
        /// 是否关闭
        /// </summary>
        public bool Status { get; set; }

        public bool IsCurrent { get; set; }
    }
}