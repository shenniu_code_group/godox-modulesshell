﻿using ShenNius.Sys.API.Domain.Entity.Common;
using SqlSugar;

namespace ShenNius.Sys.API.Domain.Entity
{
    ///<summary>
    /// 权限角色表
    ///</summary>
    [SugarTable("Sys_Role")]
    public partial class Role : BaseEntity, IAggregateRoot
    {
        public string Name { get; private set; }
        public string Description { get; private set; }
        public static Role Create(string name, string description)
        {
            Role role = new Role() { Name = name, Description = description };
            return role;
        }
        public void Modify(int id, string name, string description)
        {
            Id = id;
            Name = name;
            Description = description;
            NotifyModified();
        }
    }
}
