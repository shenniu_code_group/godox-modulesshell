﻿using ShenNius.Sys.API.Domain.Entity.Common;
using SqlSugar;

namespace ShenNius.Sys.API.Domain.Entity
{
    ///<summary>
    /// 系统菜单表
    ///</summary>
    [SugarTable("Sys_Menu")]
    public partial class Menu : BaseEntity, IAggregateRoot
    {
        public void ChangeId(int id)
        {
            Id = id;
        }
        public int ParentId { get; private set; }
        public string NameCode { get; private set; }
        public string Name { get; private set; }
        public void ChangeName(string name)
        {
            Name = name;
        }
        public string Url { get; private set; }
        public string HttpMethod { get; private set; }
        public int Sort { get; private set; }
        public string Icon { get; private set; }
        [SugarColumn(IsJson = true)]
        public string[] BtnCodeIds { get; set; }

        public string ParentIdList { get; private set; }
        public int Layer { get; private set; }

        [SugarColumn(IsIgnore = true)]
        public string BtnCodeName { get; set; }

        public void ChangeParentIdList(string parentIdList, int layer)
        {
            ParentIdList = parentIdList;
            Layer = layer;
        }

    }
}
