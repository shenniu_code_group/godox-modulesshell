﻿using ShenNius.Sys.API.Domain.Entity.Common;
using SqlSugar;
using System;

namespace ShenNius.Sys.API.Domain.Entity
{
    ///<summary>
    /// 系统操作表
    ///</summary>
    [SugarTable("Sys_Log")]
    public partial class Log : IAggregateRoot
    {

        /// <summary>
        /// Desc:唯一标号Guid
        /// </summary>           
        [SugarColumn(IsPrimaryKey = true, IsIdentity = true)]
        public int Id { get; protected set; }

        /// <summary>
        /// 应用程序
        /// </summary>
        public string Application { get; private set; }

        /// <summary>
        /// 时间
        /// </summary>
        public DateTime CreateTime { get; private set; }

        /// <summary>
        /// 日志等级
        /// </summary>
        public string Level { get; private set; }

        /// <summary>
        /// 消息内容
        /// </summary>
        public string Message { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public string Logger { get; private set; }

        /// <summary>
        /// 请求Url
        /// </summary>
        public string Callsite { get; private set; }

        /// <summary>
        /// 异常信息
        /// </summary>
        public string Exception { get; private set; }

        /// <summary>
        /// IP地址
        /// </summary>
        public string IP { get; private set; }

        /// <summary>
        /// 认证用户名
        /// </summary>
        public string UserName { get; private set; }

        /// <summary>
        /// 浏览器信息
        /// </summary>
        public string Browser { get; private set; }

    }
}
