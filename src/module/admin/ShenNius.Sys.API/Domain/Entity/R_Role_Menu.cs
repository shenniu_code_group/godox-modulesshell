﻿using ShenNius.Sys.API.Domain.Entity.Common;
using SqlSugar;

namespace ShenNius.Sys.API.Domain.Entity
{
    ///<summary>
    ///
    ///</summary>
    [SugarTable("Sys_R_Role_Menu")]
    public partial class R_Role_Menu : BaseEntity
    {
        /// <summary>
        /// Desc:
        /// Default:
        /// Nullable:False
        /// </summary>           
        public int RoleId { get; private set; }

        /// <summary>
        /// Desc:
        /// Default:
        /// Nullable:False
        /// </summary>           
        public int MenuId { get; private set; }

        /// <summary>
        /// Desc:
        /// Default:1
        /// Nullable:False
        /// </summary>           
        //public bool IsPass { get; set; } = true;
        [SugarColumn(IsJson = true)]
        public string[] BtnCodeIds { get; private set; }

        public static R_Role_Menu Create(int roleId, int menuId)
        {
            R_Role_Menu addModel = new R_Role_Menu()
            { RoleId = roleId, MenuId = menuId };
            return addModel;
        }
        public void ChangeBtnCodeIds(string[] btnCodeIds)
        {
            BtnCodeIds = btnCodeIds;
            NotifyModified();
        }
    }
}
