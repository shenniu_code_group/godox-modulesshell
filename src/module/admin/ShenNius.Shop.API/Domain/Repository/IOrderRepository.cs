﻿using ShenNius.Repository;
using ShenNius.Shop.API.Domain.Entity;
using ShenNius.Shop.API.Dtos.Output;
using ShenNius.Shop.API.Dtos.Query;
using ShenNius.Sys.API.Infrastructure.Configs;
using System.Threading.Tasks;

namespace ShenNius.Shop.API.Domain.Repository
{
    public interface IOrderRepository : IBaseRepository<Order>
    {
        Task<ApiResult> GetListPageAsync(OrderKeyListTenantQuery query);
        Task<ApiResult<OrderDetailOutput>> GetOrderDetailAsync(int orderId);
    }
}
