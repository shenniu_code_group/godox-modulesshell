﻿using ShenNius.Shop.API.Dtos.Input;
using ShenNius.Sys.API.Domain.Entity.Common;
using SqlSugar;

/*************************************
* 类名：Goods_Spec
* 作者：realyrare
* 邮箱：mahonggang8888@126.com
* 时间：2021/8/9 18:02:16
*┌───────────────────────────────────┐　    
*│　   版权所有：神牛软件　　　　	     │
*└───────────────────────────────────┘
**************************************/

namespace ShenNius.Shop.API.Domain.Entity
{
    [SugarTable("shop_Goods_Spec")]
    public class GoodsSpec : BaseTenantEntity
    {
        public int GoodsId { get; private set; }
        public string GoodsNo { get; private set; }
        public decimal GoodsPrice { get; private set; }
        /// <summary>
        /// 商品划线价
        /// </summary>
        public decimal LinePrice { get; private set; }
        public int StockNum { get; private set; }
        /// <summary>
        /// 商品销量
        /// </summary>
        public int GoodsSales { get; private set; }
        public double GoodsWeight { get; private set; }
        /// <summary>
        /// 商品spu标识
        /// </summary>
        public string SpecSkuId { get; private set; }

        public static GoodsSpec Create(GoodsSpecInput input)
        {
            GoodsSpec goodsSpec = new GoodsSpec()
            {
                TenantId = input.TenantId,
                GoodsId = input.GoodsId,
                SpecSkuId = input.SpecSkuId,
                GoodsPrice = input.GoodsPrice,
                LinePrice = input.LinePrice,
                GoodsNo = input.GoodsNo,
                StockNum = input.StockNum,
                GoodsWeight = input.GoodsWeight
            };
            return goodsSpec;
        }

    }
}