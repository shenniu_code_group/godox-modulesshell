﻿using ShenNius.Sys.API.Domain.Entity.Common;
using SqlSugar;

/*************************************
* 类名：Shop_Category
* 作者：realyrare
* 邮箱：mahonggang8888@126.com
* 时间：2021/8/9 16:51:33
*┌───────────────────────────────────┐　    
*│　   版权所有：神牛软件　　　　	     │
*└───────────────────────────────────┘
**************************************/

namespace ShenNius.Shop.API.Domain.Entity
{
    [SugarTable("shop_category")]
    public class Category : BaseTenantTreeEntity, IAggregateRoot
    {
        public void ChangeId(int id)
        {
            Id = id;
        }
        public string IconSrc { get; private set; }
        public string Name { get; private set; }
        public void ChangeName(string name)
        {
            Name = name;
        }
    }
}