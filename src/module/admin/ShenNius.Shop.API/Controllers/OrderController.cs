﻿using Microsoft.AspNetCore.Mvc;
using ShenNius.Shop.API.Domain.Repository;
using ShenNius.Shop.API.Dtos.Query;
using ShenNius.Sys.API.Controllers;
using ShenNius.Sys.API.Infrastructure.Attributes;
using ShenNius.Sys.API.Infrastructure.Configs;
using System.Threading.Tasks;

namespace ShenNius.Shop.API.Controllers
{
    /// <summary>
    /// 商品订单控制器
    /// </summary>
    [Route("api/shop/[controller]/[action]")]
    [MultiTenant]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IOrderRepository _orderRepository;

        public OrderController(IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository;
        }

        [HttpGet, Authority]
        public Task<ApiResult> GetListPages([FromQuery] OrderKeyListTenantQuery query)
        {
            return _orderRepository.GetListPageAsync(query);
        }

    }
}
