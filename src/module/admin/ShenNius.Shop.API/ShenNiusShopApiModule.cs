﻿using Microsoft.Extensions.DependencyInjection;
using ShenNius.ModuleCore;
using ShenNius.ModuleCore.Context;
using ShenNius.Shop.API.Domain.Repository;
using ShenNius.Shop.API.Infrastructure.Repository;
using ShenNius.Sys.API;

namespace ShenNius.Shop.API
{
    [DependsOn(
    typeof(ShenNiusSysApiModule)
   )]
    public class ShenNiusShopApiModule : AppModule
    {
        public override void OnConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddAutoMapper(typeof(AutomapperProfile));
            context.Services.AddScoped<IGoodsRepository, GoodsRepository>();
            context.Services.AddScoped<IOrderRepository, OrderRepository>();
        }
        public override void OnApplicationInitialization(ApplicationInitializationContext context)
        {
        }
    }
}
