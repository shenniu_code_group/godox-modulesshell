﻿using ShenNius.Sys.API.Dtos.Common;

namespace ShenNius.Shop.API.Dtos.Input
{
    public class CategoryModifyInput : GlobalTenantInput
    {
        public int Id { get; set; }
        public int ParentId { get; set; }
        public string IconSrc { get; set; }
        public string Name { get; set; }
    }
}
