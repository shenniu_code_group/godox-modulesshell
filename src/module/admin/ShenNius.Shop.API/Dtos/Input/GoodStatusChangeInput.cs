﻿using ShenNius.Sys.API.Dtos.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShenNius.Shop.API.Dtos.Input
{
    public class GoodStatusChangeInput : GlobalTenantInput
    {
        public int Id { get; set; }
        public int GoodsStatus { get; set; }
    }
}
