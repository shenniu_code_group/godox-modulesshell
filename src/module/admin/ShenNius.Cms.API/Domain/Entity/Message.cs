﻿using ShenNius.Sys.API.Domain.Entity.Common;
using SqlSugar;
namespace ShenNius.Cms.API.Domain.Entity
{

    [SugarTable("Cms_Message")]
    public class Message : BaseTenantEntity, IAggregateRoot
    {
        public int BusinessId { get; private set; }
        public string Types { get; private set; }
        public string Email { get; private set; }
        public string Content { get; private set; }
        public int UserId { get; private set; }
        public string UserName { get; private set; }
        public string IP { get; private set; }
        public int ParentId { get; private set; }
        public string Address { get; private set; }
    }
}
