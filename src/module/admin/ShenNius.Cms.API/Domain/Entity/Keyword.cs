﻿using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.DependencyInjection;
using ShenNius.Caches;
using ShenNius.Sys.API.Domain.Entity.Common;
using ShenNius.Sys.API.Infrastructure;
using ShenNius.Sys.API.Infrastructure.Common;
using SqlSugar;

/*************************************
* 类名：Keyword
* 作者：realyrare
* 邮箱：mahonggang8888@126.com
* 时间：2021/3/31 19:03:29
*┌───────────────────────────────────┐　    
*│　     版权所有：神牛软件　　　　	 │
*└───────────────────────────────────┘
**************************************/

namespace ShenNius.Cms.API.Domain.Entity
{
    /// <summary>
    /// 关键词
    /// </summary>
    [SugarTable("Cms_Keyword")]
    public class Keyword : BaseTenantEntity, IAggregateRoot
    {
        public string Title { get; private set; }
        public string Url { get; private set; }
        public static Keyword Create(string title, string url)
        {
            var _cacheHelper = MyHttpContext.Current.RequestServices.GetRequiredService<IDistributedCache>();
            var userContext = MyHttpContext.Current.RequestServices.GetRequiredService<ICurrentUserContext>();
            var tenantId = _cacheHelper.Get<int>($"{SysCacheKey.CurrentTenant}:{userContext.Id}");
            var model = new Keyword()
            {
                Title = title,
                Url = url,
                TenantId = tenantId
            };
            return model;
        }
    }
}