﻿using ShenNius.Sys.API.Dtos.Common;

/*************************************
* 类 名： ColumnInput
* 作 者： realyrare
* 邮 箱： mahonggang8888@126.com
* 时 间： 2021/3/15 19:04:32
* .netV： 3.1
*┌───────────────────────────────────┐　    
*│　     版权所有：神牛软件　　　　	 │
*└───────────────────────────────────┘
**************************************/

namespace ShenNius.Cms.API.Dtos.Input
{
    public class ColumnInput : GlobalTenantInput
    {
        public string Title { get; set; }
        public string EnTitle { get; set; }
        public string SubTitle { get; set; }
        public int ParentId { get; set; } = 0;
        public string ParentList { get; set; }
        public int Layer { get; set; }
        public string Attr { get; set; }
        public string ImgUrl { get; set; }
        public string Keyword { get; set; }
        public string Summary { get; set; }

    }
}