﻿using Newtonsoft.Json;
using ShenNius.Sys.API.Domain.Entity.Common;
using SqlSugar;
using System;
using System.Collections.Generic;

namespace ShenNius.Wechat.API.Domain.Entity
{
    ///<summary>
    /// 微信素材表
    ///</summary>
    [SugarTable("Wechat_material")]
    public class Material : BaseTenantEntity, IAggregateRoot
    {
        /// <summary>
        /// Desc:所属公众号ID
        /// Default:
        /// Nullable:False
        /// </summary>           
        public int WechatId { get; set; }

        /// <summary>
        /// Desc:类型，1=图文  2=连接
        /// Default:1
        /// Nullable:False
        /// </summary>           
        public byte Type { get; set; } = 1;

        /// <summary>
        /// Desc:保存位置  1=本地 2=服务器
        /// Default:0
        /// Nullable:False
        /// </summary>           
        public byte Position { get; set; } = 1;

        /// <summary>
        /// Desc:标题
        /// Default:
        /// Nullable:False
        /// </summary>           
        public string Title { get; set; }

        /// <summary>
        /// Desc:图片
        /// Default:
        /// Nullable:True
        /// </summary>           
        public string Img { get; set; }

        /// <summary>
        /// Desc:描述
        /// Default:
        /// Nullable:True
        /// </summary>           
        public string Summary { get; set; }

        /// <summary>
        /// Desc:作者
        /// Default:
        /// Nullable:True
        /// </summary>           
        public string Author { get; set; }

        /// <summary>
        /// Desc:连接
        /// Default:
        /// Nullable:True
        /// </summary>           
        public string Link { get; set; }

        /// <summary>
        /// Desc:内容
        /// Default:
        /// Nullable:True
        /// </summary>           
        public string Content { get; set; }

        /// <summary>
        /// Desc:内容Json
        /// Default:
        /// Nullable:True
        /// </summary>           
        public string TestJson { get; set; }

        public static Material Create(Material input)
        {
            if (string.IsNullOrEmpty(input.TestJson))
            {
                throw new ArgumentNullException(nameof(input.TestJson));
            }
            var material = new Material();
            var mList = JsonConvert.DeserializeObject<List<Material>>(input.TestJson);
            var scModel = mList[0];
            material.Title = scModel.Title;
            material.Author = scModel.Author;
            material.Img = scModel.Img;
            material.Summary = scModel.Summary;
            material.Link = scModel.Link;
            material.Content = scModel.Content;
            return material;
        }
    }
}
