﻿using Newtonsoft.Json;

namespace ShenNius.Wechat.API.Dtos
{
    public class AccessTokenOutput
    {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 过期时间
        /// </summary>
        [JsonProperty("expires_in")]
        public int ExpiresIn { get; set; }
    }
    /// <summary>
    /// 微信接口返回值
    /// </summary>
    public class WxResult
    {
        /// <summary>
        /// 返回代码  0=正确
        /// </summary>
        [JsonProperty("errcode")]
        public int Errcode { get; set; }
        /// <summary>
        /// 返货错误
        /// </summary>
        [JsonProperty("errmsg")]
        public string Errmsg { get; set; }
    }

}
