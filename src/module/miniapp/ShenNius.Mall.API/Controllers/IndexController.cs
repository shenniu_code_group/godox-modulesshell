﻿/*************************************
* 类名：IndexController
* 作者：realyrare
* 邮箱：mahonggang8888@126.com
* 时间：2021/8/30 16:48:29
*┌───────────────────────────────────┐　    
*│　   版权所有：神牛软件　　　　	     │
*└───────────────────────────────────┘
**************************************/

using Microsoft.AspNetCore.Mvc;
using ShenNius.Auth.API.Controllers;
using ShenNius.Auth.API.Dtos.Common;
using ShenNius.Auth.API.Infrastructure.Configs;
using ShenNius.Mall.API.Domain.Repository;
using SqlSugar;
using System.Threading.Tasks;

namespace ShenNius.Mall.API.Controllers
{
    /// <summary>
    /// 小程序首页
    /// </summary>
    public class IndexController : AppBaseController
    {
        private readonly IGoodsRepository _goodsRepository;
        //private readonly IAdvListRepository _advListRepository;

        public IndexController(IGoodsRepository goodsService)
        {
            _goodsRepository = goodsService;
            //  _advListRepository = advListRepository;
        }
        [HttpGet]
        public async Task<ApiResult> Page()
        {
            var query = new ListTenantQuery() { Page = 1, Limit = 4, TenantId = HttpWx.TenantId };
            var newest = await _goodsRepository.GetByWherePageAsync(query, (g, c, gc) => g.Id, OrderByType.Desc);
            query.Limit = 10;
            var best = await _goodsRepository.GetByWherePageAsync(query, (g, c, gc) => gc.GoodsSales, OrderByType.Desc);
            // var items = await _advListRepository.GetListAsync(d=>d.Type==AdvEnum.MiniApp);
            return new ApiResult(new { newest, best });
        }
    }
}