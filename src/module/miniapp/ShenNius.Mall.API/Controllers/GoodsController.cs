﻿using Microsoft.AspNetCore.Mvc;
using ShenNius.Auth.API.Controllers;
using ShenNius.Auth.API.Dtos.Common;
using ShenNius.Auth.API.Infrastructure.Configs;
using ShenNius.Mall.API.Domain.Entity;
using ShenNius.Mall.API.Domain.Repository;
using SqlSugar;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
/*************************************
* 类名：GoodsController
* 作者：realyrare
* 邮箱：mahonggang8888@126.com
* 时间：2021/8/31 14:46:34
*┌───────────────────────────────────┐　    
*│　   版权所有：神牛软件　　　　	     │
*└───────────────────────────────────┘
**************************************/

namespace ShenNius.Mall.API.Controllers
{/// <summary>
/// 商品管理
/// </summary>
    public class GoodsController : AppBaseController
    {
        private readonly IGoodsRepository _goodsRepository;

        public GoodsController(IGoodsRepository goodsRepository)
        {
            _goodsRepository = goodsRepository;
        }
        [HttpGet]
        public Task<ApiResult> BuyNow(int goodsId, int goodsNum, string goodsNo)
        {
            /*订单购买  查询商品需要知道商品的id,商品的编码*/
            //查询实体
            return _goodsRepository.GetBuyNowAsync(goodsId, goodsNum, goodsNo, HttpWx.TenantId);
        }
        [HttpGet]
        public Task<ApiResult> Detail(int goodsId)
        {
            return _goodsRepository.DetailAsync(goodsId);
        }
        [HttpGet]
        public Task<ApiResult> Lists(string sortType, int sortPrice, int categoryId)
        {
            var query = new ListTenantQuery() { Page = 1, Limit = 20, TenantId = HttpWx.TenantId };
            Expression<Func<Goods, Category, GoodsSpec, bool>> expression = (g, c, gc) => g.IsDeleted == true;

            Expression<Func<Goods, Category, GoodsSpec, object>> order = (g, c, gc) => g.CreateTime;

            OrderByType sort = OrderByType.Desc;

            if (categoryId > 0)
            {
                expression = (g, c, gc) => g.CategoryId == categoryId;
            }

            if (sortType == "all" && sortPrice == 0)
            {
                order = (g, c, gc) => g.SalesActual;
            }
            else if (sortType == "all" && sortPrice == 1)
            {
                order = (g, c, gc) => g.Id;
            }
            else if (sortType == "sales" && sortPrice == 1)
            {
                order = (g, c, gc) => gc.GoodsSales;
            }
            else if (sortType == "price" && sortPrice == 0)
            {
                order = (g, c, gc) => gc.GoodsPrice;
                sort = OrderByType.Asc;
            }
            else if (sortType == "price" && sortPrice == 1)
            {
                order = (g, c, gc) => gc.GoodsPrice;
            }

            return _goodsRepository.GetByWherePageAsync(query, order, sort, expression);

        }
    }
}