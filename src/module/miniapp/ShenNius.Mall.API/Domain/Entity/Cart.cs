﻿

using ShenNius.Auth.API.Domain.Entity.Common;
using SqlSugar;

namespace ShenNius.Mall.API.Domain.Entity
{
    [SugarTable("shop_cart")]
    public partial class Cart : BaseTenantEntity, IAggregateRoot
    {
        public int GoodsNum { get; private set; }
        public int AppUserId { get; private set; }
        public int GoodsId { get; private set; }
        public string SpecSkuId { get; private set; }

        public void AddGoodsNum(int goodsNum, string specSkuId)
        {
            GoodsNum += goodsNum;
            SpecSkuId = specSkuId;
            NotifyModified();
        }
        public void SubGoodsNum(int num)
        {
            GoodsNum -= num;
            NotifyModified();
        }
        public static Cart Create(int goodsId, int appUserId, int goodsNum, string specSkuId)
        {
            Cart model = new Cart()
            {
                GoodsId = goodsId,
                AppUserId = appUserId,
                GoodsNum = goodsNum,
                SpecSkuId = specSkuId
            };
            return model;
        }
    }
}
