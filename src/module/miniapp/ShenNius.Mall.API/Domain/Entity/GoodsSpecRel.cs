﻿
using ShenNius.Auth.API.Domain.Entity.Common;
using SqlSugar;

/*************************************
* 类名：Goods_Spec
* 作者：realyrare
* 邮箱：mahonggang8888@126.com
* 时间：2021/8/9 18:02:16
*┌───────────────────────────────────┐　    
*│　   版权所有：神牛软件　　　　	     │
*└───────────────────────────────────┘
**************************************/

namespace ShenNius.Mall.API.Domain.Entity
{
    /// <summary>
    /// 商品规格，规格值多对多的表
    /// </summary>
    [SugarTable("shop_goods_spec_rel")]
    public class GoodsSpecRel : BaseTenantEntity
    {
        private GoodsSpecRel()
        {

        }
        public int GoodsId { get; private set; }

        /// <summary>
        /// 商品规格id
        /// </summary>
        public int SpecId { get; private set; }
        /// <summary>
        /// 商品规格值id
        /// </summary>
        public int SpecValueId { get; private set; }

    }
}