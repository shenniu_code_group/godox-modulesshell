﻿using ShenNius.Auth.API.Dtos.Common;
using ShenNius.Auth.API.Infrastructure.Configs;
using ShenNius.Mall.API.Domain.Entity;
using ShenNius.Repository;
using SqlSugar;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ShenNius.Mall.API.Domain.Repository
{
    public interface IGoodsRepository : IBaseRepository<Goods>
    {
        Task<ApiResult> DetailAsync(int id);
        /// <summary>
        /// 小程序首页
        /// </summary>
        /// <param name="pageQuery"></param>
        /// <returns></returns>
        Task<ApiResult> GetByWherePageAsync(ListTenantQuery query, Expression<Func<Goods, Category, GoodsSpec, object>> orderBywhere, OrderByType sort, Expression<Func<Goods, Category, GoodsSpec, bool>> where = null);
        /// <summary>
        /// 立即购买
        /// </summary>
        /// <returns></returns>
        Task<ApiResult> GetBuyNowAsync(int goodsId, int goodsNum, string goodsNo, int tenantId);
        /// <summary>
        /// 商品信息判断（添加购物车，下单共用）
        /// </summary>
        /// <param name="goodsId"></param>
        /// <param name="goodsNum"></param>
        /// <param name="specSkuId"></param>
        /// <param name="appUserId"></param>
        /// <returns></returns>
        Task<Tuple<Goods, GoodsSpec>> GoodInfoIsExist(int goodsId, int goodsNum, string specSkuId, int appUserId);
    }
}
