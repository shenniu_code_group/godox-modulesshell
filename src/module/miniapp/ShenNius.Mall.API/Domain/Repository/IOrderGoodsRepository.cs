﻿using ShenNius.Auth.API.Infrastructure.Configs;
using ShenNius.Mall.API.Domain.Entity;
using ShenNius.Repository;
using System.Threading.Tasks;

namespace ShenNius.Mall.API.Domain.Repository
{
    public interface IOrderGoodsRepository : IBaseRepository<OrderGoods>
    {
        Task<ApiResult> ReceiptAsync(int orderId, int appUserId);
        Task<ApiResult> CancelOrderAsync(int orderId, int goodsId, int appUserId);
        Task<ApiResult> GetListAsync(int appUserId, string dataType);
        Task<ApiResult> BuyNowAsync(int goodsId, int goodsNum, string specSkuId, int appUserId);
        Task<ApiResult> CartBuyAsync(int appUserId);
    }
}
