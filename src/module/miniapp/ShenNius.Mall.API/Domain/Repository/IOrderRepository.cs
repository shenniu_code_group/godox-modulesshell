﻿using ShenNius.Auth.API.Infrastructure.Configs;
using ShenNius.Mall.API.Domain.Entity;
using ShenNius.Mall.API.Dtos.Output;
using ShenNius.Repository;
using System.Threading.Tasks;

namespace ShenNius.Mall.API.Domain.Repository
{
    public interface IOrderRepository : IBaseRepository<Order>
    {
        Task<ApiResult<OrderDetailOutput>> GetOrderDetailAsync(int orderId);
    }
}
