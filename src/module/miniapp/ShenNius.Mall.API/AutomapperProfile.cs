﻿using AutoMapper;
using ShenNius.Mall.API.Domain.Entity;
using ShenNius.Mall.API.Dtos.Output;

namespace ShenNius.Mall.API
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            //shop
            CreateMap<Goods, GoodsDetailOutput>();
        }
    }
}
