﻿using ShenNius.Auth.API.Models.Dtos.Output;

namespace ShenNius.Auth.API
{
    /// <summary>
    /// 当前用户上下文
    /// </summary>
    public class CurrentAppContext
    {
        public CurrentAppContext(HttpWxUserOutput wxUserOutput)
        {
            WxUser = wxUserOutput;
        }
        public static HttpWxUserOutput WxUser { get; set; }

    }
}
