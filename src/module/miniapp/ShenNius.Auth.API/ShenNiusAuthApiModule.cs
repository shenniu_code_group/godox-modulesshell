﻿using Microsoft.Extensions.DependencyInjection;
using ShenNius.Auth.API.Infrastructure.Common;
using ShenNius.Auth.API.Infrastructure.Configs;
using ShenNius.ModuleCore;
using ShenNius.ModuleCore.Context;

namespace ShenNius.Auth.API
{
    public class ShenNiusAuthApiModule : AppModule
    {
        public override void OnConfigureServices(ServiceConfigurationContext context)
        {
            AppConfig.AppId = context.Configuration[""];
            AppConfig.AppSecret = context.Configuration[""];

            context.Services.AddScoped<HttpHelper>();
            context.Services.AddHttpClient();
            context.Services.AddDistributedMemoryCache();
        }
        public override void OnApplicationInitialization(ApplicationInitializationContext context)
        {
        }
    }
}
