﻿namespace ShenNius.Auth.API.Domain.Entity.Common
{
    public interface IEntity
    {
        int Id { get; }
    }
}
