﻿using Microsoft.AspNetCore.Mvc;
using ShenNius.Repository;
using ShenNius.Shop.API.Domain.Repository;
using ShenNius.Sys.API.Domain.Entity;
using System.Threading.Tasks;

namespace ShenNius.Mvc.Admin.Areas.Shop.Controllers
{
    [Area("shop")]
    public class GoodsController : Controller
    {
        private readonly IGoodsRepository _goodsService;
        private readonly IBaseRepository<Config> _configService;
        public GoodsController(IGoodsRepository goodsService, IBaseRepository<Config> configService)
        {
            _goodsService = goodsService;
            _configService = configService;
        }
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public async Task<IActionResult> Add()
        {
            var datas = await _configService.GetListAsync(d => d.Type.Equals("Freight") && d.IsDeleted == false);
            ViewBag.Freights = datas;
            return View();
        }
        [HttpGet]
        public async Task<IActionResult> Modify(int id = 0)
        {
            var result = await _goodsService.DetailAsync(id);
            var datas = await _configService.GetListAsync(d => d.Type.Equals("Freight") && !d.IsDeleted);
            ViewBag.Freights = datas;

            return View(result.Data);
        }
    }
}
