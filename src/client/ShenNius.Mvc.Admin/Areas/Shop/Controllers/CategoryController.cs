﻿using Microsoft.AspNetCore.Mvc;
using ShenNius.Repository;
using ShenNius.Shop.API.Domain.Entity;
using System.Threading.Tasks;

namespace ShenNius.Mvc.Admin.Areas.Shop.Controllers
{
    [Area("shop")]
    public class CategoryController : Controller
    {
        private readonly IBaseRepository<Category> _categoryService;

        public CategoryController(IBaseRepository<Category> categoryService)
        {
            _categoryService = categoryService;
        }
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public async Task<IActionResult> Modify(int id = 0)
        {
            Category model = id == 0 ? new Category() : await _categoryService.GetModelAsync(d => d.Id == id && d.IsDeleted == false);
            return View(model);
        }
    }
}
