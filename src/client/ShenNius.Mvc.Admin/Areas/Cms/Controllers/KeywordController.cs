﻿using Microsoft.AspNetCore.Mvc;
using ShenNius.Cms.API.Domain.Entity;
using ShenNius.Repository;
using System.Threading.Tasks;

namespace ShenNius.Mvc.Admin.Areas.Cms.Controllers
{
    [Area("cms")]
    public partial class KeywordController : Controller
    {
        private readonly IBaseRepository<Keyword> _keywordService;

        public KeywordController(IBaseRepository<Keyword> KeywordService)
        {
            _keywordService = KeywordService;
        }
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public async Task<IActionResult> Modify(int id = 0)
        {
            Keyword model = id == 0 ? new Keyword() : await _keywordService.GetModelAsync(d => d.Id == id && !d.IsDeleted);
            return View(model);
        }
        [HttpGet]
        public IActionResult ImportKey()
        {
            return View();
        }

    }
}
