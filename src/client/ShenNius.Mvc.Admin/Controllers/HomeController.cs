﻿using Microsoft.AspNetCore.Mvc;
using ShenNius.Cms.API.Domain.Entity;
using ShenNius.Repository;
using ShenNius.Shop.API.Domain.Entity;
using ShenNius.Sys.API.Domain.ValueObjects;
using SqlSugar;
using System.Threading.Tasks;

namespace ShenNius.Mvc.Admin.Controllers
{
    public class HomeController : Controller
    {
        private readonly ISqlSugarClient _db;

        public HomeController(ISqlSugarClient db)
        {
            _db = db;
        }
        [HttpGet("shennius-master.html")]
        //必须设置下面该路由，不然报错404
        [Route("/")]
        public async Task<IActionResult> Index()
        {
            var value = await SysSetting.ReadAsync();
            return View(value);
        }
        public async Task<IActionResult> Report()
        {
            var articleCount=  await _db.Queryable<Article>().Where(d => !d.IsDeleted).CountAsync();
            var columnCount = await _db.Queryable<Column>().Where(d => !d.IsDeleted).CountAsync();
            var goodsCount = await _db.Queryable<Goods>().Where(d => !d.IsDeleted).CountAsync();
            var categoryCount = await _db.Queryable<Category>().Where(d => !d.IsDeleted).CountAsync();
            var orderCount = await _db.Queryable<Order>().Where(d => !d.IsDeleted).CountAsync();
            ViewBag.articleCount = articleCount;
            ViewBag.columnCount = columnCount;
            ViewBag.goodsCount = goodsCount;
            ViewBag.categoryCount = categoryCount;
            ViewBag.orderCount = orderCount;
            return View();
        }
        [HttpGet("error.html")]
        public IActionResult Error()
        {
            return View();
        }
    }
}
